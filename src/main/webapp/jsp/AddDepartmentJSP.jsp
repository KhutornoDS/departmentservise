<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

       <html>
       <head>
       <meta charset="UTF-8"/>
       <link rel="stylesheet" type ="text/css" href = "/styles/ShowStyle.css"/>
       <title>Departments</title>
       </head>
       <body>
       <h1 id="toptitle"> Добавить департамент</h1>

       <form action="NewDepartment" method="post">
         <h2 id="inputtitle"> Введите название департамента</h2>
       	  <input type= "text" name="name" value = "${name}"/>
       	     <input type="submit" value="Добавить департамент"/>
       	        </form>

       	 <h3 id="error">${message}</h3>

       	 <form action="ShowDepartments" method="post">
                    <input type="submit"value="Отмена"/>
                    	</form>
                </body>
       </html>