<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<html>
<head>
<meta charset="UTF-8"/>
<link rel="stylesheet" type ="text/css" href = "/styles/EmployeesStyle.css"/>
<title>Departments</title>
</head>
   <body>
<h1 id ="toptitle"> Список сотрудников  ${departmentName} :</h1>
<br>
 <br/>
	 <h2 id= "error">  ${message}</h2>
  <br>
  <table >
   <th><tr ><td id ="item"> Имя  </td><td id = "item">   Фамилия</td><td  id = "item">   Должность</td><td  id = "item">   Зарплата</td><td id = "item">   Дата рождения</td><td id = "item">   E-mail</td></tr></th>
    <c:forEach items="${employees}" var="item" >
	  <tr>
	   <td id = "item">${item.getFirstName()}  </td>
	    <td id = "item">${item.getSecondName()}  </td>
	     <td id = "item">${item.getPosition()}  </td>
	      <td id = "item">${item.getSalary()}  </td>
	       <td id = "item">${ item.getDateOfbirth()}  </td>
	        <td id = "item">${item.getEmail()}  </td>
	       <td><form action="ChangeEmployee" method="get">
	        <input type="hidden" name = "employeeId" value= "${item.getId()}"/>
	        <input type="hidden" name = "departmentid" value="${departmentId}"/>
                            <input type="submit"value="Редактировать"/>
                            	</form></td>
                            	<td> <form action="DeleteEmployee" method="post">
                            	 <input type="hidden" name = "departmentId" value="${departmentId}"/>
                            	 <input type="hidden" name = "id" value="${item.getId()}"/>
                                                <input type="submit"value="Удалить"/>
                                                	</form></td>
	</c:forEach>
	   </table>
   <br/>
	 </br>
	 <table>
    	<tr> <td><form action="AddEmployee" method="get">
    	         <input type="hidden" name = "departmentid" value="${departmentId}"/>
    	          <input type="hidden" name = "name" value="${departmentName}"/>
                 <input type="submit"value="Добавить"/>
                 	</form></td>
                 	<td> <form action="ShowDepartments" method="post">
                                     <input type="submit"value="Назад"/>
                                     	</form></td></tr>
                                     	</table>
</html>