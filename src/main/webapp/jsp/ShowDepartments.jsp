<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<html>
<head>
<meta charset="UTF-8"/>
<link rel="stylesheet" type ="text/css" href = "/styles/ShowStyle.css"/>
<title>Departments</title>
</head>

   <body>

<h1 id ="toptitle"> Список департаментов :</h1>
<br>
 <br/>
	  <h2 id= "error"> ${message}</h2>
  <br>
  <table>
    <c:forEach items="${departments}" var="item" >

	  <tr>
	   <td id = "item" >${item}  </td>
	   <td> <form action="ChangeDepartment"  accept-charset="UTF-8" method="get">
       	         <input type="hidden" name="id" value="${item.getId()}" />
       	       <input type="submit" value="Редактировать"/>
                  </form></td>
	   <td> <form action="DeleteDepartment" method="post">
	       <input type="hidden" name="id" value="${item.getId()}" />
	       <input type="submit" value="Удалить"/>
           </form></td>
                      <td> <form action="ShowEmployees" method="post">
                                 	       <input type="hidden" name="departmentId" value="${item.getId()}" />
                                 	       <input type="submit" value="Список сотрудников"/>
                                            </form></td>

	     </tr>

	</c:forEach>
	</table>
   <br/>


	<form action="NewDepartment" method="get" >
     <input type="submit" value="Добавить">
     </form>

</html>

