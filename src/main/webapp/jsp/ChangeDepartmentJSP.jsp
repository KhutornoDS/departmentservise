<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<html>
<head>
<meta charset="UTF-8"/>
       <link rel="stylesheet" type ="text/css" href = "/styles/ShowStyle.css"/>
<title>Departments</title>
</head>
<body>
<h1 id="toptitle"> Изменить департамент ${oldName}</h1>

<form action="ChangeDepartment" method="post">
       <h2 id="inputtitle"> Введите название департамента</h2>

	   <input type="text" name="name"  value ="${name}"/>
	    <input type="hidden" name="oldName" value = "${oldName}"/>
	   <input type="hidden" name="id" value = "${id}"/>
	   <input type="submit" value="Изменить название департамента"/>
	</form>
	 <h3 id="error">${message}</h3>
	  </br>
     	 <form action="ShowDepartments" method="post">
                  <input type="submit"value="Отмена"/>
                  	</form>
 </body>
</html>