package validation;


import dbworker.DBworker;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ValidationClass {

    public boolean emptyName(String name) {

        String numRegex = ".*[0-9].*";
        String alphaReqex = ".*[a-zA-Zа-яА-Я].*";
        if (name.matches(alphaReqex) || name.matches(numRegex)) {
            return false;
        } else {
            return true;
        }
    }

    public boolean hasNumbers(String name) {
        String numRegex = ".*[0-9].*";
        if (name.matches(numRegex)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isNumeric(String str) {
        try {
            int i = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public boolean incorectSymbol(String str) {
        String numRegex = ".*[!@#$%^*()_+\"].*";
        if (str.matches(numRegex)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDateValid(String date) {
        String DATE_FORMAT = "yyyy-MM-dd";
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public boolean isYearValid(int year) {
        Calendar now = Calendar.getInstance();
        int currentYear = now.get(Calendar.YEAR);

        if (year < currentYear && year > currentYear - 100) {
            return true;
        }
        return false;
    }

    public boolean isEmailValid(String email) {
        String mailRegex = ".*@.*";
        String symbolReqex = ".*[а-яА-Я].*";
        if (email.matches(mailRegex) && !email.matches(symbolReqex)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isMailExist(String mail) throws SQLException {
        String comand = "SELECT * FROM departments.employees where `e-mail` = '" + mail + "'";
        DBworker dBworker = new DBworker();
        if (dBworker.isExist(comand)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean sameMail(int id, String newMail) throws SQLException {
        DBworker dBworker = new DBworker();
        String mail = dBworker.showMail(id);
        if (newMail.equals(mail)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDepartmentExist(String name) throws SQLException {
        String comand = "SELECT name FROM departments.departments where name =\"" + name + "\";";
        DBworker dBworker = new DBworker();
        if (dBworker.isExist(comand)) {
            return true;
        } else {
            return false;
        }
    }
}
