package servlets;

import dbworker.DBworker;
import entities.Employee;
import validation.ValidationClass;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/ChangeEmployee")
public class ChangeEmployeeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBworker dBworker = new DBworker();
        int employeeId = Integer.parseInt(request.getParameter("employeeId"));
        int departmentId = Integer.parseInt(request.getParameter("departmentid"));
        try {
            Employee employee = dBworker.ShowEmployee(employeeId);
            String dateofBirth = String.valueOf(employee.getDateOfbirth());
            String[] date = dateofBirth.split("-");
            request.setAttribute("firstName", employee.getFirstName());
            request.setAttribute("secondName", employee.getSecondName());
            request.setAttribute("position", employee.getPosition());
            request.setAttribute("salary", employee.getSalary());
            request.setAttribute("Email", employee.getEmail());
            request.setAttribute("employeeId", employeeId);
            request.setAttribute("departmentId", employee.getDepartmentId());
            request.setAttribute("year", date[0]);
            request.setAttribute("mounth", date[1]);
            request.setAttribute("day", date[2]);
            request.setAttribute("departmentId", departmentId);
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBworker dBworker = new DBworker();
        request.setCharacterEncoding("UTF-8");
        ValidationClass validationClass = new ValidationClass();


        request.setAttribute("firstName", request.getParameter("firstName"));
        request.setAttribute("secondName", request.getParameter("secondName"));
        request.setAttribute("position", request.getParameter("position"));
        request.setAttribute("salary", request.getParameter("salary"));
        request.setAttribute("Email", request.getParameter("Email"));
        request.setAttribute("employeeId", request.getParameter("employeeId"));
        request.setAttribute("departmentId", request.getParameter("departmentId"));
        request.setAttribute("year", request.getParameter("year"));
        request.setAttribute("mounth", request.getParameter("mounth"));
        request.setAttribute("day", request.getParameter("day"));

        int employeeId = Integer.parseInt(request.getParameter("employeeId"));
        int departmentId = Integer.parseInt(request.getParameter("departmentId"));

        request.setAttribute("employeeId", employeeId);
        request.setAttribute("departmentId", departmentId);

        if (validationClass.emptyName(request.getParameter("firstName"))) {
            request.setAttribute("message", "Имя не может быть пустым");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.incorectSymbol(request.getParameter("firstName"))) {
            request.setAttribute("message", "Имя содержит запрещенные символы");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.hasNumbers(request.getParameter("firstName"))) {
            request.setAttribute("message", "Имя не может содержать цифр");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.emptyName(request.getParameter("secondName"))) {
            request.setAttribute("message", "Фамилия  не может быть пустой");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.incorectSymbol(request.getParameter("secondName"))) {
            request.setAttribute("message", "Фамилия содержит запрещенные символы");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.hasNumbers(request.getParameter("secondName"))) {
            request.setAttribute("message", "фамилия не может содержать цифр");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.emptyName(request.getParameter("position"))) {
            request.setAttribute("message", "Должность не может быть пустой");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isNumeric(request.getParameter("salary"))) {
            request.setAttribute("message", "Зарплата должна быть указана чисельно");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isNumeric(request.getParameter("year"))) {
            request.setAttribute("message", "Год должен быть указан числом");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isNumeric(request.getParameter("mounth"))) {
            request.setAttribute("message", "Месяц должен быть указан числом");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isNumeric(request.getParameter("day"))) {
            request.setAttribute("message", "День должен быть указан числом");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.emptyName(request.getParameter("Email"))) {
            request.setAttribute("message", "E-mail не может быть пустым");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isEmailValid(request.getParameter("Email"))) {
            request.setAttribute("message", "E-mail введен не верно");
            request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
        } else {
            try {
                if (validationClass.isMailExist(request.getParameter("Email")) && !validationClass.sameMail(employeeId, request.getParameter("Email"))) {
                    request.setAttribute("message", "E-mail уже существует");
                    request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
                } else {
                    String firstName = request.getParameter("firstName");
                    String secondName = request.getParameter("secondName");
                    String position = request.getParameter("position");
                    int salary = Integer.parseInt(request.getParameter("salary"));
                    int year = Integer.parseInt(request.getParameter("year"));
                    int mounth = Integer.parseInt(request.getParameter("mounth"));
                    int day = Integer.parseInt(request.getParameter("day"));
                    String email = request.getParameter("Email");
                    String dateOfbirth = year + "-" + mounth + "-" + day;
                    if (!validationClass.isYearValid(year)) {
                        request.setAttribute("message", "Год указан не верно");
                        request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
                    } else if (!validationClass.isDateValid(dateOfbirth)) {
                        request.setAttribute("message", "Дата указана не верно");
                        request.getRequestDispatcher("/jsp/ChangeEmployeeJSP.jsp").forward(request, response);
                    } else {
                        dBworker.changeEmployee(firstName, secondName, position, salary, dateOfbirth, email, employeeId);
                        ShowEmployeesServlet showEmployeesServlet = new ShowEmployeesServlet();
                        showEmployeesServlet.doPost(request, response);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
