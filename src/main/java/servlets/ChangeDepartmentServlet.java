package servlets;

import dbworker.DBworker;
import validation.ValidationClass;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/ChangeDepartment")
public class ChangeDepartmentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBworker dBworker = new DBworker();
        String oldName;
        int id = Integer.parseInt(request.getParameter("id"));
        oldName = dBworker.ShowDepartmentName(id);
        request.setAttribute("oldName", oldName);
        request.setAttribute("id", id);
        request.getRequestDispatcher("/jsp/ChangeDepartmentJSP.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ValidationClass validationClass = new ValidationClass();
        DBworker dBworker = new DBworker();
        request.setCharacterEncoding("UTF-8");
        String oldName = request.getParameter("oldName");
        String name = request.getParameter("name");
        int id = Integer.parseInt(request.getParameter("id"));

        try {
            if (validationClass.isDepartmentExist(name)) {
                request.setAttribute("name", name);
                request.setAttribute("oldName", oldName);
                request.setAttribute("id", id);
                request.setAttribute("message", "Департамент с таким названием уже существует");
                request.getRequestDispatcher("/jsp/ChangeDepartmentJSP.jsp").forward(request, response);
            } else if (validationClass.emptyName(name)) {
                request.setAttribute("name", name);
                request.setAttribute("oldName", oldName);
                request.setAttribute("id", id);
                request.setAttribute("message", "Название департамента не может быть пустым");
                request.getRequestDispatcher("/jsp/ChangeDepartmentJSP.jsp").forward(request, response);
            }  else if (validationClass.incorectSymbol(name)) {
                request.setAttribute("message", "Название департамента содержит запрещенные символы");
                request.setAttribute("name", name);
                request.getRequestDispatcher("/jsp/AddDepartmentJSP.jsp").forward(request, response);
            } else {
                dBworker.changeDepartment(id, name);
                ShowDepartmentsServlet showDepartmentsServlet = new ShowDepartmentsServlet();
                showDepartmentsServlet.doPost(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}



