package servlets;

import dbworker.DBworker;
import validation.ValidationClass;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet("/NewDepartment")
public class AddDepartmentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/AddDepartmentJSP.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBworker dBworker = new DBworker();
        ValidationClass validationClass = new ValidationClass();
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        if (validationClass.emptyName(name)) {
            request.setAttribute("message", "Название департамента не может быть пустым");
            request.setAttribute("name", name);
            request.getRequestDispatcher("/jsp/AddDepartmentJSP.jsp").forward(request, response);
        }
        if (validationClass.incorectSymbol(name)) {
            request.setAttribute("message", "Название департамента содержит запрещенные символы");
            request.setAttribute("name", name);
            request.getRequestDispatcher("/jsp/AddDepartmentJSP.jsp").forward(request, response);
        } else {
            try {
                if (validationClass.isDepartmentExist(name)) {
                    request.setAttribute("message", "Департамент с таким названием уже существует");
                    request.setAttribute("name", name);
                    request.getRequestDispatcher("/jsp/AddDepartmentJSP.jsp").forward(request, response);
                } else {
                    dBworker.addDepartment(name);
                    ShowDepartmentsServlet showDepartmentsServlet = new ShowDepartmentsServlet();
                    showDepartmentsServlet.doPost(request, response);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}

