package servlets;

import dbworker.DBworker;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/DeleteEmployee")
public class DeleteEmployeeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBworker dBworker = new DBworker();
        request.setCharacterEncoding("UTF-8");

        int id = Integer.parseInt(request.getParameter("id"));
        int departmentId = Integer.parseInt(request.getParameter("departmentId"));

        dBworker.deleteEmployee(id);

        request.setAttribute("departmentId", departmentId);
        ShowEmployeesServlet showEmployeesServlet = new ShowEmployeesServlet();
        showEmployeesServlet.doPost(request, response);


    }
}
