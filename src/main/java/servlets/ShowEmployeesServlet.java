package servlets;

import dbworker.DBworker;
import entities.Employee;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

@WebServlet("/ShowEmployees")
public class ShowEmployeesServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBworker dBworker = new DBworker();
        int departmentId = Integer.parseInt(request.getParameter("departmentId"));
        request.setAttribute("departmentId", departmentId);
        String departmentName = dBworker.ShowDepartmentName(departmentId);
        request.setAttribute("departmentName", departmentName);
        ArrayList<Employee> employees = null;
        try {
            try {
                employees = dBworker.showEmployees(departmentId);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (employees.isEmpty()) {
            request.setAttribute("message", "Список сотрудников пуст");
            request.getRequestDispatcher("jsp/ShowEmployees.jsp").forward(request, response);
        } else {
            request.setAttribute("employees", employees);
            request.getRequestDispatcher("jsp/ShowEmployees.jsp").forward(request, response);
        }

    }

}
