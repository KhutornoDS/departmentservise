package servlets;

import dbworker.DBworker;
import entities.Department;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/ShowDepartments")
public class ShowDepartmentsServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBworker dBworker = new DBworker();
        ArrayList<Department> departments = dBworker.showDepartments();
        if (departments.isEmpty()) {
            request.setAttribute("message", "Список департаментов пуст");
            request.getRequestDispatcher("jsp/ShowDepartments.jsp").forward(request, response);
        } else {
            request.setAttribute("departments", departments);
            request.getRequestDispatcher("jsp/ShowDepartments.jsp").forward(request, response);
        }

    }
}
