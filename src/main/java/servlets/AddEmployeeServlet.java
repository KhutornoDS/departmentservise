package servlets;

import dbworker.DBworker;
import validation.ValidationClass;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/AddEmployee")
public class AddEmployeeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int departmentId = Integer.parseInt(request.getParameter("departmentid"));
        request.setAttribute("departmentId", departmentId);
        request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DBworker dBworker = new DBworker();
        request.setCharacterEncoding("UTF-8");
        ValidationClass validationClass = new ValidationClass();
        int departmentId = Integer.parseInt(request.getParameter("departmentId"));
        request.setAttribute("salary", request.getParameter("salary"));
        request.setAttribute("year", request.getParameter("year"));
        request.setAttribute("mounth", request.getParameter("mounth"));
        request.setAttribute("day", request.getParameter("day"));
        request.setAttribute("departmentId", departmentId);
        String firstName = request.getParameter("firstName");
        request.setAttribute("firstName", firstName);
        String secondName = request.getParameter("secondName");
        request.setAttribute("secondName", secondName);
        String position = request.getParameter("position");
        request.setAttribute("position", position);
        String email = request.getParameter("Email");
        request.setAttribute("Email", email);

        if (validationClass.emptyName(firstName)) {
            request.setAttribute("message", "Имя не может быть пустым");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.incorectSymbol(firstName)) {
            request.setAttribute("message", "Имя содержит запрещенные символы");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.hasNumbers(firstName)) {
            request.setAttribute("message", "Имя не может содержать цифр");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.emptyName(secondName)) {
            request.setAttribute("message", "Фамилия  не может быть пустой");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.incorectSymbol(secondName)) {
            request.setAttribute("message", "Фамилия содержит запрещенные символы");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.hasNumbers(secondName)) {
            request.setAttribute("message", "фамилия не может содержать цифр");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.emptyName(position)) {
            request.setAttribute("message", "Должность не может быть пустой");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isNumeric(request.getParameter("salary"))) {
            request.setAttribute("message", "Зарплата должна быть указана чисельно");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isNumeric(request.getParameter("year"))) {
            request.setAttribute("message", "Год должен быть указан числом");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isNumeric(request.getParameter("mounth"))) {
            request.setAttribute("message", "Месяц должен быть указан числом");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isNumeric(request.getParameter("day"))) {
            request.setAttribute("message", "День должен быть указан числом");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (validationClass.emptyName(email)) {
            request.setAttribute("message", "E-mail не может быть пустым");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else if (!validationClass.isEmailValid(email)) {
            request.setAttribute("message", "E-mail введен не верно");
            request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
        } else {
            try {
                if (validationClass.isMailExist(email)) {
                    request.setAttribute("message", "E-mail уже существует");
                    request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
                } else {
                    int salary = Integer.parseInt(request.getParameter("salary"));
                    int year = Integer.parseInt(request.getParameter("year"));
                    int mounth = Integer.parseInt(request.getParameter("mounth"));
                    int day = Integer.parseInt(request.getParameter("day"));
                    String dateOfbirth = year + "-" + mounth + "-" + day;
                    if (!validationClass.isYearValid(year)) {
                        request.setAttribute("message", "Год указан не верно");
                        request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
                    } else if (!validationClass.isDateValid(dateOfbirth)) {
                        request.setAttribute("message", "Дата указана не верно");
                        request.getRequestDispatcher("/jsp/AddEmployeeJSP.jsp").forward(request, response);
                    } else {
                        dBworker.addEmployee(firstName, secondName, position, salary, dateOfbirth, email, departmentId);
                        ShowEmployeesServlet showEmployeesServlet = new ShowEmployeesServlet();
                        showEmployeesServlet.doPost(request, response);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}




