package entities;


import java.sql.Date;

public class Employee {
    private int id;
    private String firstName;
    private String secondName;
    private String position;
    private int salary;
    private Date dateOfbirth;
    private String email;
    private int departmentId;

    public Employee(int id, String firstName, String secondName, String position, int salary, Date dateOfbirth, String email, int departmentId) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.position = position;
        this.salary = salary;
        this.dateOfbirth = dateOfbirth;
        this.email = email;
        this.departmentId = departmentId;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPosition() {
        return position;
    }

    public int getSalary() {
        return salary;
    }

    public Date getDateOfbirth() {
        return dateOfbirth;
    }

    public String getEmail() {
        return email;
    }

    public int getDepartmentId() {
        return departmentId;
    }


}
