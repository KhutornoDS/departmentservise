package dbworker;

import entities.Department;
import entities.Employee;

import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;

public class DBworker {

    private String url = "jdbc:mysql://localhost:3306/departments?useSSL=false";
    private String user = "root";
    private String pass = "123456789";

    private Connection createConection() {
        Connection mycon = null;
        try {
            mycon = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mycon;
    }

    private void closeConection(Connection mycon, Statement state) {

        try {
            state.close();
            mycon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Department> showDepartments() {
        ArrayList<Department> departments = new ArrayList<>();
        try {
            Connection mycon = createConection();
            Statement state = mycon.createStatement();

            String comand = "SELECT * FROM departments.departments;";
            ResultSet rs = state.executeQuery(comand);
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");
                Department department = new Department(name, id);
                departments.add(department);
            }

            closeConection(mycon, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departments;
    }

    public String ShowDepartmentName(int id) {
        String name = null;
        try {
            Connection mycon = createConection();
            Statement state = mycon.createStatement();

            String comand = "SELECT name FROM departments.departments where id =" + id + ";";
            ResultSet rs = state.executeQuery(comand);
            while (rs.next()) {
                name = rs.getString("name");
                return name;
            }

            closeConection(mycon, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return name;
    }

    public void addDepartment(String name) {

        try {
            Connection mycon = createConection();
            Statement state = mycon.createStatement();

            String comand = "INSERT INTO `departments`.`departments` (`name`) VALUES ('" + name + "');";
            state.execute(comand);
            closeConection(mycon, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void deleteDepartment(int id) {
        try {
            Connection mycon = createConection();
            Statement state = mycon.createStatement();

            String comand = "DELETE FROM `departments`.`departments` WHERE (`id` = '" + id + "');";
            String comand2 = "DELETE FROM `departments`.`employees` WHERE (`departmentId` = '" + id + "');";
            state.execute(comand2);
            state.execute(comand);
            closeConection(mycon, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void changeDepartment(int id, String name) throws SQLException {
        Connection mycon = createConection();
        Statement state = mycon.createStatement();

        String comand = "UPDATE `departments`.`departments` SET `name` = '" + name + "' WHERE (`id` = '" + id + "');";
        state.execute(comand);
        closeConection(mycon, state);
    }

    public ArrayList<Employee> showEmployees(int departmentID) throws SQLException, ParseException {
        ArrayList<Employee> employees = new ArrayList<>();

        Connection mycon = createConection();
        Statement state = null;
        try {
            state = mycon.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String comand = "SELECT * FROM departments.employees where departmentId =" + departmentID + ";";
        ResultSet rs = state.executeQuery(comand);
        while (rs.next()) {
            int id = rs.getInt("id");
            String firstName = rs.getString("firstName");
            String secondName = rs.getString("secondName");
            String position = rs.getString("position");
            int salary = rs.getInt("salary");
            String email = rs.getString("e-mail");
            Date dateOfbirth = (Date) rs.getObject("dateOfbirth");


            Employee employee = new Employee(id, firstName, secondName, position, salary, dateOfbirth, email, departmentID);
            employees.add(employee);
        }

        closeConection(mycon, state);

        return employees;
    }

    public void addEmployee(String firstName, String secondName, String position, int salary, String dateOfbirth, String email, int departmentId) {
        try {
            Connection mycon = createConection();
            Statement state = mycon.createStatement();

            String comand = "INSERT INTO `departments`.`employees` (`firstName`, `secondName`, `position`, `salary`, `dateOfbirth`, `e-mail`, `departmentId`) " +
                    "VALUES ('" + firstName + "', '" + secondName + "', '" + position + "', '" + salary + "', '" + dateOfbirth + "', '" + email + "', '" + departmentId + "');";
            state.execute(comand);
            closeConection(mycon, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteEmployee(int id) {
        try {
            Connection mycon = createConection();
            Statement state = mycon.createStatement();

            String comand = "DELETE FROM `departments`.`employees` WHERE (`id` = '" + id + "');";
            state.execute(comand);
            closeConection(mycon, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Employee ShowEmployee(int id) throws SQLException {
        Employee employee = null;
        try {
            Connection mycon = createConection();
            Statement state = mycon.createStatement();

            String comand = "SELECT * FROM departments.employees where id = " + id + ";";
            ResultSet rs = state.executeQuery(comand);
            while (rs.next()) {
                int departmentID = rs.getInt("departmentId");
                String firstName = rs.getString("firstName");
                String secondName = rs.getString("secondName");
                String position = rs.getString("position");
                int salary = rs.getInt("salary");
                String email = rs.getString("e-mail");
                Date dateOfbirth = (Date) rs.getObject("dateOfbirth");
                employee = new Employee(id, firstName, secondName, position, salary, dateOfbirth, email, departmentID);
                return employee;
            }

            closeConection(mycon, state);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employee;

    }

    public boolean isExist(String comand) throws SQLException {
        Connection mycon = createConection();
        Statement state = null;
        try {
            state = mycon.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ResultSet rs = state.executeQuery(comand);

        while (rs.next()) {
            closeConection(mycon, state);
            return true;

        }
        closeConection(mycon, state);
        return false;
    }

    public void changeEmployee(String firstName, String secondName, String position, int salary, String dateOfbirth, String email, int employeeId) throws SQLException {
        Connection mycon = createConection();
        Statement state = mycon.createStatement();
        String comand = "UPDATE `departments`.`employees` SET `firstName` = '" + firstName + "', `secondName` = '" + secondName + "', " +
                "`position` = '" + position + "', `salary` = '" + salary + "', `dateOfbirth` = '" + dateOfbirth + "', `e-mail` = '" + email + "' WHERE (`id` = '" + employeeId + "');";
        state.execute(comand);
        closeConection(mycon, state);
    }

    public String showMail(int id) throws SQLException {
        Connection mycon = createConection();
        Statement state = mycon.createStatement();
        String comand = "SELECT `e-mail` FROM departments.employees where id = " + id + ";";
        ResultSet rs = state.executeQuery(comand);
        while (rs.next()) {
            return rs.getString("e-mail");
        }
        closeConection(mycon, state);

        return " ";

    }
}


